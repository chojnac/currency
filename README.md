## Limitations

### Cases not handled 

* Connection error presentation on UI (for now there is a message on the console)
* Slow network - code is doing a request every 1 second even if the previous connection is not finished. This could be fixed by adding a flag that skips creating a new request until the old one is in progress. 