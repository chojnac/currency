//
//
//  Created by Wojciech Chojnacki on 13/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import XCTest
@testable import Currency

class MainViewUseCaseTest: XCTestCase {
    class SpyMainViewUseCaseDelegate: MainViewUseCaseDelegate {
        var reloadDataPresentationCalled: Bool = false
        var reloadDataPresentationCallback: (() -> Void)?
        
        func reloadDataPresentation() {
            reloadDataPresentationCalled = true
            reloadDataPresentationCallback?()
        }
    }
    
    let latestRatesResponse = APIDataModel.LatestRatesResponse(base: "EUR", date: "2017-11-10", rates: [APIDataModel.Rate(code: "AUD", value: 1.5125),APIDataModel.Rate(code: "BGN", value: 1.9465),APIDataModel.Rate(code: "BRL", value: 3.7903) ])
    
    var api: FakeApplicationAPI!
    var fetchService: RatesFetcherService!
    var useCase: MainViewUseCase!
    
    override func setUp() {
        super.setUp()
        api = FakeApplicationAPI()
        api.latestRatesResponse = latestRatesResponse
        let baseCurrency = MainViewUseCase.BaseCurrencyModel(code: "EUR", amount: 10)
        fetchService = RatesFetcherService(currency: baseCurrency.code, timeInterval: 0.5, api: api) //call every 0.5sec
        useCase = MainViewUseCase(fetchService: fetchService, baseCurrency: baseCurrency)
    }
    
    override func tearDown() {
        super.tearDown()
        fetchService = nil
    }
    
    //we check if data is loaded on view appear
    func test_dataFetched_and_delegate_called() {
        let delegateSpy = SpyMainViewUseCaseDelegate()
        useCase.delegate = delegateSpy
        let fetched = self.expectation(description: "Call api")
        let delegateCalled = self.expectation(description: "Update UI")
        delegateSpy.reloadDataPresentationCallback = {
            delegateCalled.fulfill()
        }
        api.latestRatesTestCallback = { _ in
            //get initial data and stop timer
            self.fetchService.stop()
            fetched.fulfill()
        }
        
        useCase.onDidAppear()
        waitForExpectations(timeout: 2, handler: nil)
        
        XCTAssertEqual(latestRatesResponse.rates.count, useCase.numberOfCells())
    }
    
    //we check if data is loaded on view appear
    func test_dataFetched_and_rate_multipied_with_amount() {
        //prepare expected data (code -> rate * amount)
        var expected: [String: Decimal] = [:]
        let amount = useCase.baseCurrency.amount
        
        latestRatesResponse.rates.forEach {
            expected[$0.code] = $0.value * amount
        }
        
        //setup
        let delegateSpy = SpyMainViewUseCaseDelegate()
        useCase.delegate = delegateSpy
        let delegateCalled = self.expectation(description: "Update UI")
        delegateSpy.reloadDataPresentationCallback = {
            delegateCalled.fulfill()
            //get initial data and stop timer
            self.fetchService.stop()
        }
        
        useCase.onDidAppear()
        waitForExpectations(timeout: 2, handler: nil)
        
        XCTAssertEqual(latestRatesResponse.rates.count, useCase.numberOfCells())
        for i in 0..<useCase.numberOfCells() {
            let model = useCase.cellModel(cellForRowAt: IndexPath(item: i, section: 1))
            XCTAssertEqual(expected[model.code], model.value)
        }
    }
    
    // much more cases to test ...

}
