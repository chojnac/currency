//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import XCTest
@testable import Currency

class APIResponseMapperTest: XCTestCase {
    let jsonRawData: Data = """
{"base":"EUR","date":"2017-11-10","rates":{"AUD":1.5125,"BGN":1.9465,"BRL":3.7903}}
""".data(using: String.Encoding.utf8)!
    
    lazy var testJson: [String: Any] = {
       return try! JSONSerialization.jsonObject(with: jsonRawData, options: []) as! [String: Any]
    }()
    
    let expectedResult = APIDataModel.LatestRatesResponse(base: "EUR", date: "2017-11-10", rates: [APIDataModel.Rate(code: "AUD", value: 1.5125),APIDataModel.Rate(code: "BGN", value: 1.9465),APIDataModel.Rate(code: "BRL", value: 3.7903) ])
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_mapFromJSON() {
        var errorResult: Error?
        var result: APIDataModel.LatestRatesResponse?
        
        do {
            result = try APIResponseMapper().mapFromJSON(json: testJson)
        } catch {
            errorResult = error
        }
        
        XCTAssertNil(errorResult)
        XCTAssertNotNil(result)
        if let result = result {
            XCTAssertEqual(expectedResult, result)
        }
    }
    
    
}
