//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation
@testable import Currency

class FakeApplicationAPI: APIProtocol {
    var latestRatesResponse: APIDataModel.LatestRatesResponse?
    var latestRatesRequestCount: Int = 0
    var latestRatesTestCallback: ((_ base: String) -> Void)?
    
    func fetchLatestRates(base: String, complete: @escaping (Result<APIDataModel.LatestRatesResponse>) -> Void) {
        latestRatesRequestCount += 1
        latestRatesTestCallback?(base)
        
        guard let latestRatesResponse = latestRatesResponse else {
            complete(Result.failure(NSError(domain: "test", code: 999, userInfo: [NSLocalizedDescriptionKey: "Missing fake response "])))
            return
        }
        
        complete(Result.success(latestRatesResponse))
    }  
}
