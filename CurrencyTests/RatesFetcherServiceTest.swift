//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import XCTest
@testable import Currency

class RatesFetcherServiceTest: XCTestCase {
    class SpyRatesFetcherServiceDelegate: RatesFetcherServiceDelegate {
        var data: [APIDataModel.Rate]?
        var isError: Bool?
        var delegateCalledCallback: (() -> Void)?
        
        func didFetch(fetcher: RatesFetcherService, data: [APIDataModel.Rate]) {
            self.data = data
            self.isError = false
            delegateCalledCallback?()
        }
        func didFetch(fetcher: RatesFetcherService, withError: Error) {
            self.isError = true
            delegateCalledCallback?()
        }
    }
    
    var api: FakeApplicationAPI!
    var service: RatesFetcherService!
    
    override func setUp() {
        super.setUp()
        api = FakeApplicationAPI()
        service = RatesFetcherService(currency: "EUR", timeInterval: 0.5, api: api) //call every 0.5sec
    }
    
    override func tearDown() {
        service = nil
    }
    
    
    func test_start() {
        service.start()
        XCTAssertTrue(service.isRunning) //is running
    }
    
    func test_fetch() {
        let fetched = self.expectation(description: "test_fetch")
        api.latestRatesTestCallback = { (base: String) in
            fetched.fulfill()
        }
        service.start()
        waitForExpectations(timeout: 2, handler: nil)
        
        XCTAssertTrue(api.latestRatesRequestCount > 0)
    }
    
    //check if base currency change works
    func test_fetch_correctBaseCurrency() {
        var bases: [String] = []
        let fetched = self.expectation(description: "Fetch started")
        api.latestRatesTestCallback = { [weak self] (base: String) in
            bases.append(base)
            if base == "GBP" {
                self?.service.stop()
                fetched.fulfill()
            } else {
                self?.service.currency = "GBP"
            }
        }
        service.start()
        waitForExpectations(timeout: 2)
        XCTAssertEqual(bases, ["EUR","GBP"])
    }
    
    func test_stop() {
        let waitForFetch = self.expectation(description: "Waiting")
        waitForFetch.isInverted = true
        
        service.start()
        service.stop()
        api.latestRatesTestCallback = { (base: String) in
            waitForFetch.fulfill()
        }
        
        var noMoreRequests = false
        waitForExpectations(timeout: 1) { (error) in
            noMoreRequests = true
        }
        
        XCTAssertTrue(noMoreRequests)
    }
    
    func test_delegate_didFetchWithError() {
        let waitForFetch = self.expectation(description: "Waiting")
        let waitForDelegate = self.expectation(description: "Waiting for delegate")
        let delegate = SpyRatesFetcherServiceDelegate()
        service.delegate = delegate
        delegate.delegateCalledCallback = {
            waitForDelegate.fulfill()
        }
        api.latestRatesTestCallback = { [weak self] (base: String) in
            waitForFetch.fulfill()
            self?.service.stop()
        }
        service.start()
        waitForExpectations(timeout: 2, handler: nil)
        
        guard let isError = delegate.isError else {
            XCTFail("Delegate not called")
            return
        }
        
        XCTAssertTrue(isError)
    }
    
    func test_delegate_didFetchData() {
        let waitForFetch = self.expectation(description: "Waiting")
        
        let testData = APIDataModel.LatestRatesResponse(base: "EUR", date: "2011-01-01", rates: [APIDataModel.Rate(code: "GBP", value: 1.99)])
        api.latestRatesResponse = testData
        
        let delegate = SpyRatesFetcherServiceDelegate()
        
        service.delegate = delegate
        
        api.latestRatesTestCallback = { [weak self] (base: String) in
            waitForFetch.fulfill()
            self?.service.stop()
        }
        service.start()
        waitForExpectations(timeout: 2, handler: nil)
        
        guard let isError = delegate.isError,
            let responseRates = delegate.data else {
            XCTAssert(false, "Delegate not called")
            return
        }
        
        XCTAssertFalse(isError)
        XCTAssertEqual(responseRates, testData.rates)
    }
    
}
