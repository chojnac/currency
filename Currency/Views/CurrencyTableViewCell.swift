//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import UIKit

protocol CurrencyTableViewCellDelegate: class {
    func valueChanged(cell: CurrencyTableViewCell, value: Decimal)
}

class CurrencyTableViewCell: UITableViewCell {
    static var cellReuseIdentifier: String {
        return "CurrencyTableViewCell"
    }
    
    static var cellNib: UINib {
        return UINib(nibName: "CurrencyTableViewCell", bundle: nil)
    }
    
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var valueInput: UITextField!
    
    weak var delegate: CurrencyTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        valueInput.delegate = self
    }

    func configure(model: CurrencyTableViewCellModel) {
        codeLabel.text = model.name
        valueInput.text = model.formatedValue
        valueInput.isEnabled = model.isEditing
    }
    
    func focusOnInput() {    
        valueInput.becomeFirstResponder()
    }
    
}

extension CurrencyTableViewCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = (textField.text ?? "") as NSString
        var textAfterChange = text.replacingCharacters(in: range, with: string)
        
        if textAfterChange == "" {
            textAfterChange = "1"
        }
        
        if textAfterChange.rangeOfCharacter(from: CharacterSet(charactersIn:"0123456789.,").inverted) != nil {
            return false
        }
        
        if let value = Decimal(string: textAfterChange) {
            self.delegate?.valueChanged(cell: self, value: value)
            return true
        }
        
        return false
    }
}
