//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

struct CurrencyTableViewCellModel {
    let code: String
    let value: Decimal
    var isEditing: Bool
    
    let formatedValue: String
    let name: String
    
    init(code: String, value: Decimal, isEditing: Bool) {
        self.code = code
        self.name = code
        self.value = value
        self.isEditing = isEditing
        let fmt = NumberFormatter()
        fmt.maximumFractionDigits = 2
        fmt.minimumFractionDigits = 0
        formatedValue = fmt.string(from: value as NSNumber)!
    }
    
}
