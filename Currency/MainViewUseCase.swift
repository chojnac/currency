//
//
//  Created by Wojciech Chojnacki on 13/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//
import Foundation

protocol MainViewUseCaseDelegate: class {
    func reloadDataPresentation()
}

final class MainViewUseCase {
    struct BaseCurrencyModel {
        var code: String
        var amount: Decimal
    }
    
    private let fetchService: RatesFetcherService
    private(set) var baseCurrency: BaseCurrencyModel
    private var rates: [APIDataModel.Rate] = []
    
    weak var delegate: MainViewUseCaseDelegate?
    
    init(fetchService: RatesFetcherService, baseCurrency: BaseCurrencyModel) {
        self.fetchService = fetchService
        self.baseCurrency = baseCurrency
        
        fetchService.delegate = self
    }
    
    func updateAmount(_ amount: Decimal) {
        baseCurrency.amount = amount
        delegate?.reloadDataPresentation()
    }
    
    func updateData(data: [APIDataModel.Rate]) {
        self.rates = data
        delegate?.reloadDataPresentation()
    }
    
    func cellModel(cellForRowAt indexPath: IndexPath) -> CurrencyTableViewCellModel {
        if indexPath.section == 0 {
            return CurrencyTableViewCellModel(code: baseCurrency.code, value: baseCurrency.amount, isEditing: true)
        }
        let rate = rates[indexPath.row]
        return CurrencyTableViewCellModel(code: rate.code, value: rate.value * baseCurrency.amount, isEditing: false)
    }
    
    func numberOfCells() -> Int {
        return rates.count
    }
    
    func selectCell(at index: Int) {
        let model = rates[index]
        let baseModel = APIDataModel.Rate(code: baseCurrency.code, value: 1/model.value)
        rates.remove(at: index)
        rates.insert(baseModel, at: 0)
        fetchService.currency = model.code
        baseCurrency = BaseCurrencyModel(code: model.code, amount: model.value * baseCurrency.amount)
    }
}


extension MainViewUseCase: ScreenLifecycle {
    func onDidLoad() {
        
    }
    
    func onDidAppear() {
        fetchService.start()
    }
    
    func onDidDisappear() {
        fetchService.stop()
    }
}

extension MainViewUseCase: RatesFetcherServiceDelegate {
    func didFetch(fetcher: RatesFetcherService, data: [APIDataModel.Rate]) {
        updateData(data: data)
    }
    func didFetch(fetcher: RatesFetcherService, withError error: Error) {
        print("error: \(error)")
    }
}
