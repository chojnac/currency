//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
