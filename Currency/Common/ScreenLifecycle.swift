//
//
//  Created by Wojciech Chojnacki on 13/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

protocol ScreenLifecycle {
    func onDidLoad()
    func onDidAppear()
    func onDidDisappear()
}
