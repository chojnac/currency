//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

protocol RatesFetcherServiceDelegate: class {
    func didFetch(fetcher: RatesFetcherService, data: [APIDataModel.Rate])
    func didFetch(fetcher: RatesFetcherService, withError error: Error)
}

public class RatesFetcherService {
    private let timeInterval: TimeInterval
    private let api: APIProtocol
    private var timer: Timer?
    
    
    var currency: String
    var isRunning: Bool {
        return timer != nil
    }
    
    weak var delegate: RatesFetcherServiceDelegate?
    
    init(currency: String, timeInterval: TimeInterval, api: APIProtocol) {
        self.currency = currency
        self.timeInterval = timeInterval
        self.api = api
    }
    
    func start() {
        guard timer == nil else {
            return
        }
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true, block: { [weak self] (timer) in
            self?.tick()
        })
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    private func tick() {
        api.fetchLatestRates(base: currency) { (response) in
            DispatchQueue.main.async {
                switch response {
                case .success(let data):
                    self.delegate?.didFetch(fetcher: self, data: data.rates)
                case .failure(let error):
                    self.delegate?.didFetch(fetcher: self, withError: error)
                    break
                }
            }
        }
    }
}
