//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
   
    var useCase: MainViewUseCase!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //initialize table
        tableView.register(CurrencyTableViewCell.cellNib, forCellReuseIdentifier: CurrencyTableViewCell.cellReuseIdentifier)
        
        //initialize service
        let baseCurrency = MainViewUseCase.BaseCurrencyModel(code: "GBP", amount: Decimal(10))
        let fetchService = RatesFetcherService(currency: baseCurrency.code, timeInterval: 1, api: API.shared)
        useCase = MainViewUseCase(fetchService: fetchService, baseCurrency: baseCurrency)
        useCase.onDidLoad()
        useCase.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        useCase.onDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        useCase.onDidDisappear()
    }
    
}

// MARK: - MainViewUseCaseDelegate
extension MainViewController: MainViewUseCaseDelegate {
    func reloadDataPresentation() {
        tableView.reloadSections(IndexSet([1]), with: .none)
    }
}

// MARK: - CurrencyTableViewCellDelegate
extension MainViewController: CurrencyTableViewCellDelegate {
    func valueChanged(cell: CurrencyTableViewCell, value: Decimal) {
        useCase.updateAmount(value)
    }
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section > 0 else {
            return
        }
        tableView.beginUpdates()
        let cellNew = tableView.cellForRow(at: indexPath) as! CurrencyTableViewCell
        let cellBase = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! CurrencyTableViewCell
        
        tableView.moveRow(at: IndexPath(row: 0, section: 0), to: IndexPath(row: 0, section: 1))
        tableView.moveRow(at: indexPath, to: IndexPath(row: 0, section: 0))
        
        useCase.selectCell(at: indexPath.row)
        
        configure(cell: cellNew, at: IndexPath(row: 0, section: 0))
        configure(cell: cellBase, at: IndexPath(row: 0, section: 1))

        cellNew.focusOnInput()
        
        tableView.endUpdates()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return useCase.numberOfCells()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
    UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyTableViewCell.cellReuseIdentifier, for: indexPath) as! CurrencyTableViewCell
        configure(cell: cell, at: indexPath)
        return cell
    }
    
    func configure(cell: CurrencyTableViewCell, at indexPath: IndexPath) {
        let model = useCase.cellModel(cellForRowAt: indexPath)
        cell.configure(model: model)
        if model.isEditing {
            cell.delegate = self
        } else {
            cell.delegate = nil
        }
    }
}
