//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

struct APIDataModel{
    struct Rate: Equatable {
        let code: String
        let value: Decimal
        
        static func ==(lhs: Rate, rhs: Rate) -> Bool {
            return lhs.code == rhs.code && lhs.value == rhs.value
        }
    }
    
    struct LatestRatesResponse: Equatable {
        let base: String
        let date: String
        let rates: [Rate]
        
        static func ==(lhs: LatestRatesResponse, rhs: LatestRatesResponse) -> Bool {
            return lhs.base == rhs.base && lhs.date == rhs.date && lhs.rates == rhs.rates
        }
    }
}
