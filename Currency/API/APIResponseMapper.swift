//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

class APIResponseMapper {
    
    func mapFromJSON(json: [String: Any]) throws -> APIDataModel.LatestRatesResponse  {
        guard let base = json["base"] as? String,
            let date = json["date"] as? String,
            let rates = json["rates"] as? [String: Double] else {
                throw APIError.objectSerialization(reason: "Invalid json")
        }
        
        var ratesArray: [APIDataModel.Rate] = []
        
        for item in rates {
            ratesArray.append(APIDataModel.Rate(code: item.key, value: Decimal(item.value)))
        }
        ratesArray.sort { (a, b) -> Bool in
            return a.code < b.code
        }
        return APIDataModel.LatestRatesResponse(base: base, date: date, rates: ratesArray)
    }
    
}
