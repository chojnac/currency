//
//
//  Created by Wojciech Chojnacki on 12/11/2017.
//  Copyright © 2017 Chojnacki. All rights reserved.
//

import Foundation

protocol APIProtocol {
    func fetchLatestRates(base: String, complete: @escaping (Result<APIDataModel.LatestRatesResponse>) -> Void)
}
enum APIError: Error {
    case objectSerialization(reason: String)
}

class API: APIProtocol {
    static var shared: APIProtocol {
        return API()
    }
    
    func fetchLatestRates(base: String, complete: @escaping (Result<APIDataModel.LatestRatesResponse>) -> Void) {
        var urlComponents = URLComponents(string: "https://revolut.duckdns.org/latest")!
        urlComponents.queryItems = [
            URLQueryItem(name: "base", value: base)
        ]
        let request = URLRequest(url: urlComponents.url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error  {
                complete(Result.failure(error))
                return
            }
            
            guard let responseData = data else {
                complete(Result.failure(APIError.objectSerialization(reason: "No data in response")))
                return
            }
            
            do {
                guard
                    let json = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    else {
                    complete(Result.failure(APIError.objectSerialization(reason: "Invalid json")))
                    return
                }
                let latestRatesResponse = try APIResponseMapper().mapFromJSON(json: json)
                complete(Result.success(latestRatesResponse))
            } catch {
                complete(Result.failure(APIError.objectSerialization(reason: "Invalid json")))
            }
        }
        
        task.resume()
    }
    
}


